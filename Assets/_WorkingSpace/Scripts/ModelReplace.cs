using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelReplace : MonoBehaviour {
    public Image body, fx;
    public Sprite[] listBody, listFX;

    public enum status { idle, correct, failed }
    public void PlayerStatus(status s) {
        int index = 0;
        switch (s) {
            case status.idle:
                index = 0;                
                break;
            case status.correct:
                index = 1;
                break;
            case status.failed:
                index = 2;
                break;
        }
        body.sprite = listBody[index];
        fx.sprite = listFX[index];
        fx.color = Color.white;
    }
}
