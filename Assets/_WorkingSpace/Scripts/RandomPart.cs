﻿using UnityEngine;
using DG.Tweening;

public class RandomPart : MonoBehaviour {
    public Vector3[] positions;
    bool isReadyToCheck;

    void Start() {
        isReadyToCheck = false;
    }

    public void RandomMoveToPart() {
        isReadyToCheck = false;
        partID = RandomID();
        transform.DOLocalMove(positions[partID], .5f).OnComplete(()=> isReadyToCheck = true);
    }

    int RandomID() {
        int r = Mathf.FloorToInt(Random.Range(0, positions.Length));
        if(r != partID) {
            return r;
        } else {
            return RandomID();
        }
    }

    public int GetStatusAndID() {
        if(isReadyToCheck){
            return partID;
        } else {
            return -1;
        }
    }

    int partID {
        get; set;
    }
}
