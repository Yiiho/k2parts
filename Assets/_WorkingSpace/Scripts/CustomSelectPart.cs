using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CustomSelectPart : MonoBehaviour {
    public Image itemImg;
    public TextMeshProUGUI itemTxt;
    public GameObject showNonActiveLayer;
    public Collider coll;
    int itemID;
    public int GetItemID()=>itemID;

    public void SetData(BoneItemData itemData, int id) {
        itemID = id;
        itemImg.sprite = itemData.img;
        itemTxt.text = itemData.name_th;
    }

    public void SetNonActive(){
        coll.enabled = false;
        showNonActiveLayer.SetActive(true);
    }
}
