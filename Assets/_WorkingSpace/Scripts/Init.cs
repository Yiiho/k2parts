﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Init : MonoBehaviour {
    KinectManager manager;
    CustomObjectOverlay handOverlay;
    Collider handCollider;
    public Image startImageFill;
    public Image fadeObject;
    public Collider startCollider;
    public GameObject howtoObj;
   
    private bool isInitCompleted = false;

    void Start() {
        //startImageFill.SetActive(false);
        fadeObject.color = new Color(1, 1, 1, 1);
        fadeObject.DOFade(0, .3f).OnComplete(()=> isInitCompleted = true);

        manager = KinectManager.Instance;
        handOverlay = (Global.ins.objectOverlay != null) ?
            Global.ins.objectOverlay :
            Global.ins.objectOverlay = manager.gameObject.GetComponent<CustomObjectOverlay>();
        handCollider = (Global.ins.overlayCollider != null) ?
            Global.ins.overlayCollider :
            Global.ins.overlayCollider = manager.GetComponentInChildren<Collider>();

    }
    long userId;
    void Update() {
        if (isInitCompleted) {
            if (manager && manager.IsInitialized()) {
                userId = manager.GetPrimaryUserID();
                if (userId == 0) {
                    howtoObj.SetActive(false);
                } else {
                    howtoObj.SetActive(true);
                    if (startCollider.bounds.Intersects(handCollider.bounds)) {
                        startImageFill.fillAmount += (Time.deltaTime);
                        if (startImageFill.fillAmount >= 1) {
                            startImageFill.fillAmount = 0;
                            Global.ins.PlaySFX(2);
                            Global.ins.OnLoadSceneAt(2);
                            return;
                        }
                    } else {
                        startImageFill.fillAmount = 0;
                    }
                }
            }
        }
    }
}
