using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InfoGroup : MonoBehaviour {
    public TextMeshProUGUI textHeader, textDescription;
    public Image imageItem;
    public Image imageItem_descript;

    public void SetBoneData(BoneItemData boneData) {
        textHeader.text = boneData.name_th + "<br>" + "("+ boneData.name_en + ")";
        textDescription.text = boneData.description_th;
        imageItem.sprite = boneData.img;
        imageItem_descript.sprite = boneData.img_description_th;
    }
    
}
