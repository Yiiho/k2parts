﻿using System;
using UnityEngine;

public class HandSwapper : MonoBehaviour {
    public SpriteRenderer spr;
    public Sprite[] t2d;

    public GameObject holdItemLayer;
    public SpriteRenderer sprHoldItem;
    int currentID;
    bool handStateStatus;

    void Start() {
        ChangeHandeState(false);
    }
   
    public void ChangeHandeState(bool isCloseHand, Sprite sp=null, int id=-1) {
        handStateStatus = isCloseHand;
        spr.sprite = t2d[Convert.ToInt32(isCloseHand)];
        if (sp) {
            holdItemLayer.SetActive(true);
            sprHoldItem.sprite = sp;
            currentID = id;
        } else {
            holdItemLayer.SetActive(false);
        }
    }

    public bool GetHandState() {
        return handStateStatus;
    }

    public int GetHoldItemID() {
        return currentID;
    }
    
}
