﻿public class TwoDigit {
    public static string GetTwoDigit(int num) {
        string s = (num < 10) ? "0" + num : num.ToString();
        return s;
    }

    public static string GetFiveDigit(int num) {
        string s = ""+num;
        while(s.Length < 5) {
            s = "0" + s;
        }
        return s;
    }
}
