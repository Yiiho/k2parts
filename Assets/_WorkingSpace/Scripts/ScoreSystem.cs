﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class ScoreSystem : MonoBehaviour {
    public delegate void ScoreSystemDispatcher();
    public event ScoreSystemDispatcher timeComp;

    public string appName = "KinectWaste";
    public TextMeshProUGUI txtTimer, txtScore;
    public GameObject scoreUpIcon,wrongIcon;
//    public GameObject clock; /////////////
//    public Image clockwise; /////////////
    private int currentScore = 0;
    //  public Canvas mainCanvas;
    //  public GameObject textPopup;
    int currentTimer;
    bool isPlayerGetNewHi;
    IEnumerator cTimer;

    void Start() {
    //    clock.SetActive(false);
       // LoadHighScore();
        txtTimer.text = "" + Global.ins.config._interval;
    }

    public int MaxTimer {
        get; set;
    }

    public int CurrentScore {
        get => currentScore;
    }

    public void StartTimer() {
        currentTimer = Global.ins.config._interval;
        OnTimer();
    }

    void OnTimer() {
        cTimer = TimeDecrease();
        print(""+cTimer);
        StartCoroutine(cTimer);
    }

    private IEnumerator TimeDecrease() {
        yield return new WaitForSeconds(1.0f);
        currentTimer--;
        //currentTimer++; ///////////////////////
        ShowAndCheckTimer();
        
    }

    void ShowTimer() {
        if (currentTimer < 0) currentTimer = 0;
     //   if (currentTimer > MaxTimer) currentTimer = MaxTimer;
        txtTimer.text = TwoDigit.GetTwoDigit(currentTimer);
    //    clockwise.fillAmount = UpdateClockPercent();
    }

    float UpdateClockPercent() { /////////////////////////
     //   print(currentTimer +" tt "+ maxTimer +" mt"+currentTimer / maxTimer+"pc");
        return (float)currentTimer / (float)MaxTimer;
    }

    bool CheckTimerComp() {
        bool b;
        return b = (currentTimer <= 0) ?true:false;
        //return b = (currentTimer >= MaxTimer) ? true : false; /////////////////////
    }
    
    void ShowAndCheckTimer() {
        ShowTimer();
        if (CheckTimerComp()) {
        //    clock.SetActive(false);
            StopCoroutine(cTimer);
            TimerCompleted();
        } else {
            OnTimer();
        }
    }

    void TimerCompleted() {
        if (currentScore > hiScore) {
            isPlayerGetNewHi = true;
            hiScore = currentScore;
            SaveHighScore();
        } else {
            isPlayerGetNewHi = false;
        }
        if (timeComp != null) {
            timeComp();
        }
    }

    public void DoScore(int point, Vector3 v3) {
        if (point > 0) {
            currentScore += point;
            UpdateScoreText();
            CreatePointAtPoint(v3);
        } else {
            CreateWrongAtPoint(v3);
        }
    }

    private void CreateWrongAtPoint(Vector3 v3) {
        //currentTimer -= 2;
        currentTimer += 2;  ///////////////////////
        ShowTimer();
        if (CheckTimerComp() == true) {
        //    print(CheckTimerComp() + " ------------");
            StopCoroutine(cTimer);
            TimerCompleted();
        }
        GameObject go =  Instantiate(wrongIcon);
        go.transform.position = new Vector3(v3.x, v3.y, go.transform.position.z);
    }

    void UpdateScoreText() {
        txtScore.text = TwoDigit.GetFiveDigit(currentScore);
    }

    void CreatePointAtPoint(Vector3 v3) {
        //   Vector3 screenPoint = Camera.main.WorldToScreenPoint(v3);
        //   FloatText floatText = Instantiate(textPopup).GetComponent<FloatText>();
        //   floatText.transform.position = v3;

        //  floatText.transform.SetParent(mainCanvas.transform, false);
        // floatText.SetText("+1");
     //   GameObject go = Instantiate(scoreUpIcon);
     //   go.transform.position = new Vector3(v3.x, v3.y, go.transform.position.z);
    }
    int hiScore;

    private void SaveHighScore() {/*
        PlayerPrefs.SetString("AppName", appName);
        PlayerPrefs.SetInt("HiScore", hiScore);
        PlayerPrefs.Save();*/
    }

    public void LoadHighScore() {
        /*
        if (PlayerPrefs.HasKey("AppName")) {
            hiScore = PlayerPrefs.GetInt("HiScore");
            txtScore.text = "HI SCORE : " + TwoDigit.GetTwoDigit(hiScore);
        } else {
            hiScore = 0;
            SaveHighScore();
        }*/
        txtScore.text = "";
        txtTimer.text = "";
    }

    public int GetScore() => currentScore;
    public int GetHiScore() => hiScore; ////////////////////////
    public bool GetPlayerNewHi() => isPlayerGetNewHi; /////////////////////
}
