﻿using System.IO;
using UnityEngine;
using SimpleJSON;

public class LoadConfigFile : MonoBehaviour {
    public string path;
    private void Start() {
        path = Application.dataPath + "/../Files/Configure.json";
        GetLoadInterval();
    }

    private void GetLoadInterval() {        
        string s = File.ReadAllText(path);
        JSONNode jsonNode = SimpleJSON.JSON.Parse(s);
        int interval = int.Parse(jsonNode["interval"]);
        float blendX = float.Parse(jsonNode["blendX"]);
        float blendY = float.Parse(jsonNode["blendY"]);
        Config conf = new Config(interval, blendX, blendY);

        Global.ins.OnLoadConfigCompleted(conf);
    }
}

[System.Serializable]
public class Config {
    public int _interval;
    public float _blendX, _blendY;
    public Config(int interval, float blendX, float blendY) {
        _interval = interval;
        _blendX = blendX;
        _blendY = blendY;
    }
}

