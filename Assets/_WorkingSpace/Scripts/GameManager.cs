﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using TMPro;

public class GameManager : MonoBehaviour {
    public Text countdownTxt;
    public ScoreSystem scoreSystem;
    public GameObject tutorialGO;
    public Collider itemCollider;
    public RandomPart randomPart;
    public Image doctorSp;
    [Tooltip("0=false, 1=true, 2=idle")]
    public Sprite[] doctorImageAll;

    public GameObject[] objectsEnableWhenPlay;
    public TextMeshProUGUI drText;
    public GameObject drBubble;

    public bool isPlaying = false;
    public ModelReplace modelReplace;
    public Image fadeImg;
    HandSwapper handSwap;
    KinectManager manager;
    Config config;
    CustomObjectOverlay handOverlay;
    Collider handCollider;

    IsTrueType itemType;
    void Start() {
        Cursor.visible = false;
        config = Global.ins.config;

        manager = KinectManager.Instance;
        handOverlay = (Global.ins.objectOverlay != null) ?
            Global.ins.objectOverlay :
            Global.ins.objectOverlay = manager.gameObject.GetComponent<CustomObjectOverlay>();
        handCollider = (Global.ins.overlayCollider != null) ?
            Global.ins.overlayCollider :
            Global.ins.overlayCollider = manager.GetComponentInChildren<Collider>();
        handSwap = handCollider.GetComponent<HandSwapper>();
        handSwap.ChangeHandeState(false);
        countdownTxt.text = "";
        OnShowTutorial();
        isPlaying = false;
    }

    void OnShowTutorial() {
        DOVirtual.DelayedCall(3f, StartChallenge);
    }

    long userId;
    void Update() {
        if (manager && manager.IsInitialized()) {
            userId = manager.GetPrimaryUserID();
            if (isPlaying) {
                /*
                if (itemCollider.bounds.Intersects(handCollider.bounds)) {
                    if (randomPart.GetStatusAndID() != -1 && handSwap.dragItem.gameObject.activeSelf) {
                        if(randomPart.GetStatusAndID() == handSwap.dragItem.ID) {
                            print("Correct");
                            scoreSystem.DoScore(1000, handCollider.transform.position);
                            OnDoctorAction(true);
                            modelReplace.PlayerStatus(ModelReplace.status.correct);
                        } else {
                            print("Wrong");
                            OnDoctorAction(false);
                            modelReplace.PlayerStatus(ModelReplace.status.failed);
                        }
                        handSwap.ChangeHandeState(false);
                        //handSwap.gameObject.SetActive(false);
                    } else { //random part not ready

                    }
                }*/
            } else if (!isPlaying) {

            }

        }
    }

    void OnDoctorAction(bool isTrue) {
        drBubble.SetActive(true);
        drText.text = (isTrue) ? "ถูกต้อง": "ยังไม่ถูกนะ";
        isPlaying = false;
        DOTween.Kill(doctorSp);
        doctorSp.DOFade(0, .15f).OnComplete(() => {
            doctorSp.sprite = doctorImageAll[Convert.ToInt32(isTrue)];
            doctorSp.DOFade(1, .15f).OnComplete(()=> {
                OnDoctorIdle(2.0f);
                DOVirtual.DelayedCall(1.5f, () => drBubble.SetActive(false));
            });
        });
    }

    void OnDoctorIdle(float delayT) {
        doctorSp.DOFade(0, .15f).OnComplete(() => {
            doctorSp.sprite = doctorImageAll[2];
            doctorSp.DOFade(1, .15f);
            randomPart.RandomMoveToPart();
            isPlaying = true;
        }).SetDelay(delayT);
    }
    /*
    void CheckChildContain() {
        if (hand.transform.childCount > 1) return;

        handSwap.ChangeHandeState(true);
        itemCollider.transform.position = new Vector3(
            handCollider.transform.position.x, handCollider.transform.position.y, handCollider.transform.position.z + .1f);
        itemCollider.transform.parent = handCollider.transform;
    }
    */
    int count;
    void StartChallenge() {
        DisableTutorialPage();
        count = 4;
        countdownTxt.text = count.ToString();
        UpdateCountText();
    }

    private void DisableTutorialPage() {
        tutorialGO.SetActive(false);
    }

    int textSize = 300;
    void UpdateCountText() {
        textSize = 300;
        count--;
        if (count > 0) {
            countdownTxt.text = count.ToString();
            DOTween.To(() => textSize, x => textSize = x, 150, 1f).OnUpdate(UpdateTextSize).OnComplete(UpdateCountText);
        } else {
            countdownTxt.text = "START";
            DOTween.To(() => textSize, x => textSize = x, 150, 1f).OnUpdate(UpdateTextSize).OnComplete(StartGamePlay);
        }
    }

    void UpdateTextSize() {
        countdownTxt.fontSize = textSize;
    }

    void StartGamePlay() {
        foreach(GameObject go in objectsEnableWhenPlay) {
            go.SetActive(true);
        }
        randomPart.RandomMoveToPart();
        countdownTxt.text = "";
        scoreSystem.StartTimer();
        isPlaying = true;
        //  SpawnItem();

        scoreSystem.timeComp += StopGamePlay;
    }

    void StopGamePlay() {
        isPlaying = false;
        handSwap.ChangeHandeState(false);

        Global.ins.playerScore = scoreSystem.GetScore();
        fadeImg.DOFade(1, 1).OnComplete(() => ShowEndGame());
    }

    void ShowEndGame() {
        //  DOVirtual.DelayedCall(6f, ResetGame);
        Global.ins.OnLoadSceneAt(4);
    }

    void ResetGame() {
        countdownTxt.text = "";
        scoreSystem.LoadHighScore();
    }


    void CreateFX(int id, Transform t) {/*
        GameObject go = Instantiate(fx[id], t.position, t.rotation);
        go.transform.localScale = new Vector3(.3f, .3f, .3f);
        */
    }

    float binRotTime = .5f;

    void RotateZero(Transform trans) {
        trans.DOLocalRotate(new Vector3(0, 0, 0), binRotTime);
    }

    void CheckMatchType(IsTrueType iType, IsTrueType bType) {
        bool isCorrect = (iType == bType);
        int correctPoint = Convert.ToInt32(isCorrect);
        /*
        scoreSystem.DoScore(correctPoint, prefabGO.transform.position);
        correctObj.sprite = correctIcon[correctPoint];
        correctObj.color = new Color(correctObj.color.r, correctObj.color.g, correctObj.color.b, 1);
        correctObj.DOFade(0, 3f);*/
    }
}


[System.Serializable]
public class ImageGroup {
    public Collider collider;
    public IsTrueType type;
}
