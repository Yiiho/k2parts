using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameManagerNew : MonoBehaviour
{
    KinectManager manager;
    CustomObjectOverlay handOverlay;
    HandSwapper handSwap;
    Collider handCollider;

    public CustomSelectPart[] selectableParts;
    public Collider closeInfoBuntton;
    List<int> randomCircleCount;

    public GameObject circleCirsor;
    Collider circleCircleColl;
    public Image circleLoader;

    public GameObject infoGroupLayer;
    InfoGroup infoGroupData;

    public Transform itemForTween;
    Image itemForTweenImgContainer;

    public Animator childAnim;

    GroupRandomData groupData; 
    void Start() {
        PlayStateStatus = false;
        manager = KinectManager.Instance;
        handOverlay = (Global.ins.objectOverlay != null) ?
            Global.ins.objectOverlay :
            Global.ins.objectOverlay = manager.gameObject.GetComponent<CustomObjectOverlay>();
        handCollider = (Global.ins.overlayCollider != null) ?
            Global.ins.overlayCollider :
            Global.ins.overlayCollider = manager.GetComponentInChildren<Collider>();
        handSwap = handCollider.GetComponent<HandSwapper>();
        handSwap.ChangeHandeState(false);

        groupData =  Global.ins.GetGroupRandomData();
        circleCircleColl = circleCirsor.GetComponent<Collider>();

        infoGroupData = infoGroupLayer.GetComponent<InfoGroup>();

        itemForTweenImgContainer = itemForTween.GetChild(0).GetComponent<Image>();

        randomCircleCount = new List<int>();
        for(int i =0; i<selectableParts.Length; i++){
            selectableParts[i].SetData(groupData.boneItemDatas[i], i);
            randomCircleCount.Add(i);
        }
        randomCircleCount = Shuffle(randomCircleCount);
        GetAndMoveCircle();

        DOVirtual.DelayedCall(5f, () => PlayStateStatus = true);
    }

    private void GetAndMoveCircle() {
        if (randomCircleCount.Count > 0) {
            circleCirsor.transform.DOLocalMove(groupData.fixedPosition[randomCircleCount[0]], .3f);
            //circleCirsor.transform.localPosition = groupData.fixedPosition[randomCircleCount[0]];
            currentIndexRandom = randomCircleCount[0];
            randomCircleCount.RemoveAt(0);
        } else {
            DOTween.KillAll();
            Global.ins.OnLoadSceneAt(4);
        }
    }   

    int currentIndexRandom;
    
    long userId;
    // Update is called once per frame
    float cLoader = 0;
    CustomSelectPart selectedPart, partHoldInHand;
    void Update() {
        if (manager && manager.IsInitialized()) {
            userId = manager.GetPrimaryUserID();
            UpdateInterval();
            circleLoader.transform.position = handCollider.transform.position;

            if (PlayStateStatus) { // check PlayStateStatus

                if (circleCircleColl.bounds.Intersects(handCollider.bounds)) { //check hand intersect with circlecursor.
                    if (handSwap.holdItemLayer.activeSelf) {
                        //To check id hold in hand and id of circlecursor.
                        if (handSwap.GetHoldItemID() == currentIndexRandom) {
                            childAnim.SetTrigger("Happy");
                            Global.ins.PlaySFX(1);
                            PlayStateStatus = false;
                            partHoldInHand.SetNonActive();
                            infoGroupData.SetBoneData(groupData.boneItemDatas[currentIndexRandom]);
                            itemForTweenImgContainer.sprite = groupData.boneItemDatas[currentIndexRandom].img;
                            itemForTween.localScale = new Vector2(0.15f, 0.15f);
                            itemForTween.position = circleCircleColl.transform.position;
                            itemForTween.gameObject.SetActive(true);
                            itemForTween.DOMove(Vector3.zero, 1);
                            itemForTween.DOScale(Vector3.one, 1).OnComplete(() => EnableInfo(true));
                        } else {
                            childAnim.SetTrigger("Hurt");
                            Global.ins.PlaySFX(0);
                        }
                        handSwap.ChangeHandeState(false);
                        handSwap.holdItemLayer.SetActive(false);
                        return;
                    }
                } else { //Not intersects circlecursor then check collision to any object.
                    foreach (CustomSelectPart p in selectableParts) {
                        if (p.coll.enabled) {
                            if (p.coll.bounds.Intersects(handCollider.bounds)) {
                                if (selectedPart != null) {
                                    if (selectedPart == p) {
                                        cLoader += Time.deltaTime;
                                        circleLoader.fillAmount = cLoader;
                                        if (cLoader >= 1.0f) {
                                            if(partHoldInHand != p) Global.ins.PlaySFX(2);
                                            handSwap.ChangeHandeState(true, p.itemImg.sprite, p.GetItemID());
                                            partHoldInHand = p;
                                        }
                                    } else {
                                        cLoader = 0;
                                        circleLoader.fillAmount = cLoader;
                                        selectedPart = p;
                                        //handSwap.ChangeHandeState(false);
                                    }
                                } else {
                                    cLoader = 0;
                                    circleLoader.fillAmount = cLoader;
                                    selectedPart = p;
                                    // handSwap.ChangeHandeState(false);
                                }
                                return;
                            }
                        }
                    }
                }

            } 
            else {
                if (closeInfoBuntton.bounds.Intersects(handCollider.bounds)) {
                    cLoader += Time.deltaTime;
                    circleLoader.fillAmount = cLoader;
                    if (cLoader >= 1.0f) {
                        Global.ins.PlaySFX(2);
                        cLoader = 0;
                        PlayStateStatus = true;
                        infoGroupLayer.SetActive(false);
                        // itemForTween.gameObject.SetActive(false);
                        GetAndMoveCircle();
                    }
                    return;
                } 
                    
            } 

            cLoader = 0;
            circleLoader.fillAmount = cLoader;
           // handSwap.ChangeHandeState(false);
        }
    }

    void EnableInfo(bool isEnable) {
        infoGroupLayer.SetActive(isEnable);
        itemForTween.gameObject.SetActive(false);
        /*
        if (isEnable) {
            DOVirtual.DelayedCall(10,   ()=> {
                PlayStateStatus = true;
                infoGroupLayer.SetActive(false);
               // itemForTween.gameObject.SetActive(false);
                GetAndMoveCircle();
            });
        }*/
    }

    float intervalTimer = 0;
    void UpdateInterval() {
        if (userId == 0) {
            intervalTimer += Time.deltaTime;
            if (intervalTimer >= Global.ins.config._interval) {
                Global.ins.OnLoadSceneAt(1);
            }
        } else {
            intervalTimer = 0;
        }
    }

    bool PlayStateStatus {
        get; set;
    }

    List<int> Shuffle(List<int> _s) {
        List<int> dum = new List<int>();
        while (_s.Count > 0) {
            int rnd = Random.Range(0, _s.Count);
            dum.Add(_s[rnd]);
            _s.RemoveAt(rnd);
        }
        return dum;
    }
}
