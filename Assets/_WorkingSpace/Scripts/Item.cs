﻿using UnityEngine;
using DG.Tweening;

public class Item : MonoBehaviour {
    public ItemGroup[] items;
    SpriteRenderer sp;
    int face = 0;

    void Start() {
        sp = GetComponent<SpriteRenderer>();
    //    Invoke("RandomFace", 1f);
    }
    
    public void RandomFace() {
        DOTween.To(() => face, x => face = x, items.Length-1, 1.5f).OnUpdate(UpdateFace).OnComplete(GetFace);
    }

    void UpdateFace() {
        sp.sprite = items[face].icon;
    }

    void GetFace() {
      //  face = Mathf.FloorToInt(Random.Range(0, items.Length));
      //  UpdateFace();
      //  Collider coll = this.gameObject.AddComponent<BoxCollider>();
    }

    public int GetFaceID() {
        return face;
    }

    private void OnDestroy() {
        DOTween.Kill(this);
    }

    public IsTrueType GetTrueType() {
        return items[face].type;
    }
}

public enum IsTrueType { Yes, No };
[System.Serializable]
public class ItemGroup {
    public Sprite icon;
    public IsTrueType type;
}
