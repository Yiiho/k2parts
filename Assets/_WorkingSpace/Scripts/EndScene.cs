using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class EndScene : MonoBehaviour {
    public TextMeshProUGUI scoreTxt;
    public Image tryAgainButton;
    public Collider tryAgainCollider;
    int score, maxScore;
    KinectManager manager;
    CustomObjectOverlay handOverlay;
    Collider handCollider;

    void Start() {
        //  score = 0;
        //  maxScore = Global.ins.playerScore;
        //maxScore = 10000;
        //  DOVirtual.DelayedCall(2f, AnimScore);
        //  DOVirtual.DelayedCall(6f, () => Global.ins.OnLoadSceneAt(1));

        manager = KinectManager.Instance;
        handOverlay = (Global.ins.objectOverlay != null) ?
            Global.ins.objectOverlay :
            Global.ins.objectOverlay = manager.gameObject.GetComponent<CustomObjectOverlay>();
        handCollider = (Global.ins.overlayCollider != null) ?
            Global.ins.overlayCollider :
            Global.ins.overlayCollider = manager.GetComponentInChildren<Collider>();
    }

    private void AnimScore() {
        DOTween.To(() => score, x => score = x, maxScore, 1f).OnUpdate(UpdateUI);
    }

    void UpdateUI() {
        scoreTxt.text = SetTextDigit(score, 5);
    }

    string SetTextDigit(int num, int lenghtString) {
        string s = ""+num;
        while(s.Length < lenghtString) {
            s = "0" + s;
        }
        return s;
    }

    long userId;
    void Update() {
        if (manager && manager.IsInitialized()) {
            userId = manager.GetPrimaryUserID();
            UpdateInterval();
            if (tryAgainCollider.bounds.Intersects(handCollider.bounds)) {
                tryAgainButton.fillAmount -= (Time.deltaTime);
                if (tryAgainButton.fillAmount <= 0) {
                    tryAgainButton.fillAmount = 0;
                    Global.ins.PlaySFX(2);
                    Global.ins.OnLoadSceneAt(1);
                    return;
                }
            } else {
                tryAgainButton.fillAmount = 1;
            }
        }
        
    }

    float intervalTimer = 0;
    void UpdateInterval() {
        if (userId == 0) {
            intervalTimer += Time.deltaTime;
            if (intervalTimer >= Global.ins.config._interval) {
                Global.ins.OnLoadSceneAt(1);
            }
        } else {
            intervalTimer = 0;
        }
    }
}
