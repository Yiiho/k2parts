﻿using UnityEngine;
using UnityEngine.UI;

public class DragItem : MonoBehaviour {
    SpriteRenderer spr;

    void Start() {
        spr = GetComponent<SpriteRenderer>();
    }

    public void SetImage(Sprite sp, int id) {
        if (spr.sprite != sp) {
            ID = id;
            spr.sprite = sp;
        }
    }

    public int ID {
        get; private set;
    }
}
