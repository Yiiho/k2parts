﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Global : MonoBehaviour {
    public static Global ins;
    public AudioClip[] sfxs;
    public AudioSource audS;

    public GroupRandomData[] groupRandomDatas;
  
    void Awake() {
        ins = this;
    }

    void Start() {
        DontDestroyOnLoad(this);
        Cursor.visible = false;
        Invoke("SetFullScreen", 2.5f);
    }

    void SetFullScreen() {
        Screen.fullScreen = true;
    }

    public void OnLoadConfigCompleted(Config conf) {
        config = conf;
        OnLoadSceneAt(1);
    }

    public void OnLoadSceneAt(int index) {
        SceneManager.LoadScene(index);
    }

    public Config config {
        get; private set;
    }

    public int userTotalScore {
        get; set;
    }

    public CustomObjectOverlay objectOverlay {
        get; set;
    }

    public Collider overlayCollider {
        get; set;
    } 

    public int playerScore {
        get; set;
    }

    public GroupRandomData GetGroupRandomData(){
        return groupRandomDatas[Random.Range(0, groupRandomDatas.Length)];
    }


    public void PlaySFX(int id) {
        audS.clip = sfxs[id];
        audS.Play();
    }
}

[System.Serializable]
public struct GroupRandomData {
    public BoneItemData[] boneItemDatas;
    public Vector3[] fixedPosition;
}