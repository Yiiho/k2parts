using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SetMappingAsset : MonoBehaviour {
    public BoneItemData boneItemData;
    public TextMeshProUGUI textName;
    public Image img;

    void Start() {
        textName.text = boneItemData.name_th;
        img.sprite = boneItemData.img;
    }

}
