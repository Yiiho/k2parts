﻿using UnityEngine;
using UnityEngine.UI;

public class FloatText : MonoBehaviour {
    public Animator animator;
    private Text txt;

    void Start() {
        AnimatorClipInfo[] aInfo = animator.GetCurrentAnimatorClipInfo(0);
        Destroy(gameObject, aInfo[0].clip.length);
        txt = animator.GetComponent<Text>();
    }

    public void SetText(string s) {
        txt.text = s;
    }
}
