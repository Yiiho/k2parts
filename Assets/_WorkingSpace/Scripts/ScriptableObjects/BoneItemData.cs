using UnityEngine;

[CreateAssetMenu(fileName = "New Bone", menuName = "BoneItem")]
public class BoneItemData : ScriptableObject {
    public string name_th;
    public string name_en;
    public string description_th;
    public string description_en;
    public Sprite img;

    public Sprite img_description_th;
    public Sprite img_description_en;
}
